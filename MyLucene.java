package Task2_4;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.jsoup.Jsoup;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MyLucene {
	public static void main(String[] args) throws Exception {	
		
		// Get path or use default path
		String path = args.length < 0 ? args[0]: "D:/workspace/TextData/";
	    
		// Add documents' path to a list 
	    List<File> docList = listFile(path);
	    
	    // Create in-memory directory, index and analyzer
	    StandardAnalyzer analyzer = new StandardAnalyzer();
		Directory index = new RAMDirectory();
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		IndexWriter w = new IndexWriter(index, config);
		
		// Add documents to index directory
	    for (File doc : docList){
	    	addDoc(w, doc);
	    }
	    w.close();
	    
	    // Start searching
	    Scanner scan = new Scanner(System.in);
	    String query = "";
	    Query q;
	    int hitsPerPage = 10; // How many results are shown in a page
	    
	    // Create index objects for searching
	    IndexReader reader = DirectoryReader.open(index);
	    IndexSearcher searcher = new IndexSearcher(reader);
	    TopScoreDocCollector collector; // Results
	    ScoreDoc[] hits; // Store hit results
	    Document d; // Temporary document
	    
	    // Continue searching until enter a specific key
	    while(true){
	    	try{
	    		// User enter word
		    	System.out.println("Please enter a searching word (Enter '!q' to exit):");
		    	query = scan.nextLine();
		    	// !q to exit
		    	if (query.equals("!q")){
		    		break;
		    	}
		    	// One alphabet does not really matter
		    	else if(query.length() < 2){
		    		continue;
		    	}
		    	// Analyze docs body text
		    	q = new QueryParser("body", analyzer).parse(query);
		    	collector = TopScoreDocCollector.create(hitsPerPage); // Collect results and limit results
		    	searcher.search(q, collector); // Search in the directory
		    	hits = collector.topDocs().scoreDocs; // Get results
		    	// Some outputs for the user
		    	if (hits.length == 0){
		    		System.out.println("Sorry, I can't find anything. :(");
		    	}else if (hits.length == 1){
		    		System.out.println("A document is found!");
		    	}else{
		    		System.out.println(hits.length + " documents are found!");
		    	}
		    	// Display documents to the user
			    for(int i=0; i<hits.length; ++i){
			    	int docId = hits[i].doc;
			    	d = searcher.doc(docId);
			    	System.out.println((i+1) + ". " + d.get("title") + "\n   " + d.get("path") + "\n");
			    }
	    	}
	    	// In case of errors so we can pass through
	    	catch (Exception e){
	    		System.out.println("Sorry! Something's wrong!");
	    		System.out.println(e);
	    	}
	    }
	    // End the process
	    reader.close();	
	    System.out.println("Bye!");
	}

	// Get all text and HTML files from a directory
	private static List<File> listFile(String directoryName) throws Exception{
		File directory = new File(directoryName);
		List<File> resultList = new ArrayList<File>();
		File[] fileList = directory.listFiles();
		
		// Add every files ended with .txt or .html. If it is a directory, then uses a recursion to that directory.
		// We can add documents to the index directory here
		for (File file : fileList){
			if (file.isFile() && (file.getName().endsWith(".txt") || file.isFile() && file.getName().endsWith(".html"))){
				resultList.add(file); // Found a doc; add!
			}else if (file.isDirectory()){ // Skip directory
				resultList.addAll(listFile(file.getAbsolutePath())); // Found directory; recursion!
			}
		}
		return resultList;
	}
	
	// Add every files to a storage for searching
	private static void addDoc(IndexWriter w, File file) throws Exception {
		String title;
		String body = "";
		if (file.getName().endsWith(".txt")){
			title = file.getName().substring(0, file.getName().length()-4); // Get filename as a title
			byte[] encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath())); // Read text in the file
			body = new String(encoded, StandardCharsets.UTF_8);
		}else{
			File html = new File(file.getAbsolutePath());
		    org.jsoup.nodes.Document htmlDoc = Jsoup.parse(html, "UTF-8"); // HTML parser
		    title = htmlDoc.title();
		    body = htmlDoc.text();
		}
		Document doc = new Document();
		doc.add(new TextField("title", title, Field.Store.YES));
		doc.add(new TextField("body", body, Field.Store.YES));
		doc.add(new StringField("path", file.getAbsolutePath(), Field.Store.YES));
		w.addDocument(doc); // Add document to index
	}
}
